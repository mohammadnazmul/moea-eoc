/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moea.eoc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.gaeocframework.ensemble.EvaluateEnsembleOfClassifier;
import org.moeaframework.Analyzer;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.problem.EOC.MOEoC;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author mohammad
 */
public class RunMOEoC {

    static boolean isDebug = true;
    static boolean isEval = false;
    static boolean isFullModel = false;
    static boolean isStatCmp = false;
    static int nRun=30;
    
    static String expName, headerTxt;

    public static int kFolds = 10;
    static Instances[] trnCvFold;
    static Instances[] tstCvFold;
    static String cvModelPath, cvDataPath;

    static List<Classifier> classifierModels = new ArrayList<Classifier>();
    static Instances trnFullData;
    static Instances tstFullData;
    static String modelPath, resPath;

    public static String relationName;
    static String strTrnFile, strTstFile; // 1 has been replaced by "+datSetNo+"

    static Classifier[][] clsfCVModels; //= new Classifier[10][nClassifier]; 
    // [fold] x [nClsf]

    public static String[] clsfNames = new String[]{
        "BayesNet", "DecisionStump", "DecisionTable", "HoeffdingTree", "IBk",
        "J48", "JRip", "KStar", "LibSVM", "Logistic",
        "LWL", "NaiveBayes", "OneR", "PART", "RandomTree",
        "REPTree", "SGD", "SimpleLogistic", "VotedPerceptron", "ZeroR"
    };

    /*     
     public static Classifier[] models = {
     new BayesNet(),   new DecisionStump(),    new DecisionTable(),    new HoeffdingTree(),    new IBk(),
     new J48(),        new JRip(),             new KStar(),            new LibSVM(),           new Logistic(),       
     new LWL(),        new NaiveBayes(),       new OneR(),             new PART(),             new RandomTree(),        
     new REPTree(),    new SGD(),              new SimpleLogistic(),   new VotedPerceptron(),  new ZeroR()
     };
     */
    static PrintStream original = new PrintStream(System.out);
    static int nClassifier = clsfNames.length;

    static void loadDatasets(String cvData, int nf)
            throws Exception {
        trnCvFold = new Instances[nf];
        tstCvFold = new Instances[nf];

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(
                strTrnFile);
        trnFullData = source.getDataSet();
        System.out.println("Training Set loaded: " + strTrnFile);
        trnFullData.setClassIndex(trnFullData.numAttributes() - 1);
        relationName = trnFullData.relationName();

        if (isEval == true) {
            source = new ConverterUtils.DataSource(strTstFile);
            tstFullData = source.getDataSet();
            System.out.println("Testing Set loaded: " + strTstFile);
            tstFullData.setClassIndex(tstFullData.numAttributes() - 1);
        }
        System.out.println("Loading CV data from : " + cvData);
        for (int i = 0; i < nf; i++) {
            source = new ConverterUtils.DataSource(cvData + "train-fold-" + i
                    + ".arff");
            Instances trn = source.getDataSet();
            trn.setClassIndex(trn.numAttributes() - 1);
            trnCvFold[i] = trn;

            source = new ConverterUtils.DataSource(cvData + "test-fold-" + i
                    + ".arff");
            Instances tst = source.getDataSet();
            tst.setClassIndex(tst.numAttributes() - 1);
            tstCvFold[i] = tst;
        }
    }

    static void buildDatasets(String dsPath, String cvData, int nf)
            throws Exception {
        trnCvFold = new Instances[nf];
        tstCvFold = new Instances[nf];

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(dsPath
                + strTrnFile);
        trnFullData = source.getDataSet();
        trnFullData.setClassIndex(trnFullData.numAttributes() - 1);
        relationName = trnFullData.relationName();

        Random rand = new Random(1); // create seeded number generator
        Instances randData = new Instances(trnFullData); // create copy of
        // original data
        randData.randomize(rand); // randomize data with number generator

        for (int i = 0; i < nf; i++) {
            String fileName = cvData + "test-fold-" + i + ".arff";
            System.out.println(fileName);
            Instances train = randData.trainCV(nf, i);
            ArffSaver saver = new ArffSaver();
            saver.setInstances(train);
            saver.setFile(new File(fileName));
            saver.writeBatch();
            trnCvFold[i] = train;

            fileName = cvData + "test-fold-" + i + ".arff";
            System.out.println(fileName);
            Instances test = randData.testCV(nf, i);
            saver = new ArffSaver();
            saver.setInstances(test);
            saver.setFile(new File(fileName));
            saver.writeBatch();
            tstCvFold[i] = test;
        }
    }

    static void loadCVModels(String cvPath) throws Exception {
        if (isDebug) {
            System.out.println("Start: Loading Prebuilt Cross Validation Models...\nModels Path: " + cvPath);
        }

        clsfCVModels = new Classifier[kFolds][nClassifier];
        for (int f = 0; f < kFolds; f++) {
            if (isDebug) {
                System.out.print("\nFold " + (f + 1) + ": ");
            }
            for (int ci = 0; ci < nClassifier; ci++) {
                String clsfPath = cvPath + clsfNames[ci] + "-" + f + ".model";
                Classifier cls = (Classifier) weka.core.SerializationHelper
                        .read(clsfPath);
                clsfCVModels[f][ci] = cls;
                if (isDebug) {
                    System.out.print(clsfNames[ci] + ", ");
                }
            } // END:for nClsf
        }// END: for fold
        if (isDebug) {
            System.out
                    .println("\nFinish: Loading Prebuilt Cross Validation Models...");
        }
    }

    public static void loadSettings(String fileName) throws Exception {

        BufferedReader br = null;

        File fin = new File(fileName);
        br = new BufferedReader(new FileReader(fin));
        String line = null;
        String delims = "[ \t:]+";
        headerTxt = "";

        while ((line = br.readLine()) != null) {
            if (line.startsWith("expName:") == true) {
                String[] tokens = line.split(delims);
                expName = tokens[1];
                headerTxt += ("expName: " + expName + "\n");
            } else if (line.startsWith("trnFile:") == true) {
                String[] tokens = line.split(delims);
                strTrnFile = tokens[1];
                headerTxt += ("trnFile: " + strTrnFile + "\n");
            } else if (line.startsWith("cvDataPath:") == true) {
                String[] tokens = line.split(delims);
                cvDataPath = tokens[1];
                headerTxt += ("cvDataPath: " + cvDataPath + "\n");
            } else if (line.startsWith("cvModelPath:") == true) {
                String[] tokens = line.split(delims);
                cvModelPath = tokens[1];
                headerTxt += ("cvModelPath: " + cvModelPath + "\n");
            } else if (line.startsWith("tstFile:") == true) {
                String[] tokens = line.split(delims);
                strTstFile = tokens[1];
                isEval = true;
                headerTxt += ("tstFile: " + strTstFile + "\n");
            } else if (line.startsWith("modelPath:") == true) {
                String[] tokens = line.split(delims);
                isFullModel = true;
                modelPath = tokens[1];
                headerTxt += ("modelPath: " + modelPath + "\n");
            } else if (line.startsWith("fold:") == true) {
                String[] tokens = line.split(delims);
                kFolds = Integer.parseInt(tokens[1]);
                headerTxt += ("fold: " + kFolds + "\n");
            } else if (line.startsWith("resPath:") == true) {
                String[] tokens = line.split(delims);
                resPath = tokens[1];
            } else if (line.startsWith("statCmp:") == true) {
                isStatCmp = true;
                String[] tokens = line.split(delims);
                nRun = Integer.parseInt(tokens[1]);
                headerTxt += ("statCmp: " + nRun + "\n");
            }
        }
        br.close();
    }

    public static int solnLength(String theCmb) {
        int len = 0;
        for (int i = 0; i < theCmb.length(); i++) {
            if (theCmb.charAt(i) == '1') {
                len++;
            }
        }
        return len;
    }

    public static void testCombination(String theCmb) throws Exception {
        original.println("Evaluating: " + theCmb);  // output to stdout

        EvaluateEnsembleOfClassifier eoc = new EvaluateEnsembleOfClassifier(false, modelPath, trnFullData, tstFullData, clsfNames);
        eoc.testModel(theCmb);
        Evaluation eval = eoc.getTestEvaluation();
        int tp = (int) eval.numTruePositives(0);
        int tn = (int) eval.numTrueNegatives(0);
        int fp = (int) eval.numFalsePositives(0);
        int fn = (int) eval.numFalseNegatives(0);

        System.out.println(theCmb + "\t" + solnLength(theCmb) + "\t" + eval.precision(0) + "\t" + eval.pctCorrect() + "\t" + eval.fMeasure(0) + "\t" + eval.matthewsCorrelationCoefficient(0) + "\t" + tp + "\t" + tn + "\t" + fp + "\t" + fn);
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

        String fname = "";
        // Step 1: load Experime Settings and Variables from File
        if (args.length == 0) {
            System.err.println("\nUsage: java -jar MOEA-EoC.jar -i <input file containing the settings>\n");
            System.exit(1);
        } else if (args[0].equals("-i")) {
            fname = args[1];
            loadSettings(fname);
        } else {
            System.err.println("\nUsage: java -jar MOEA-EoC.jar -i <input file containing the settings>\n");
            System.exit(1);
        }

        // Step 2: load Datasets
        System.out.println("Loading Datasets...");  // output to stdout
        loadDatasets(cvDataPath, kFolds);

        // Step 3: Load Models
        System.out.println("Loading Models...");  // output to stdout
        loadCVModels(cvModelPath);

        if (isStatCmp == true) {
            StatCmp();
        } else {

            // Step 4: Solve the MOEoC problem with 20 base classifiers
            original.println("\nExecuting NSGAII...");  // output to stdout
            String NSGAIIresFile = resPath + expName + "-NSGAII-ConfMatrix.txt";
            File f = new File(NSGAIIresFile);
            if (f.exists()) {
                original.println("Already Executed: NSGA II on " + expName);
            } else {
                NSGAIIexecute(NSGAIIresFile);
            }

            original.println("\nExecuting NSGAIII...");  // output to stdout
            String NSGAIIIresFile = resPath + expName + "-NSGAIII-ConfMatrix.txt";
            f = new File(NSGAIIIresFile);
            if (f.exists()) {
                original.println("Already Executed: NSGAIII on " + expName);
            } else {
                NSGAIIIexecute(NSGAIIIresFile);
            }

            original.println("\nExecuting eMOEA...");  // output to stdout
            String eMOEAresFile = resPath + expName + "-eMOEA-ConfMatrix.txt";
            f = new File(eMOEAresFile);
            if (f.exists()) {
                original.println("Already Executed: eMOEA on " + expName);
            } else {
                eMOEAexecute(eMOEAresFile);
            }
        }
        //  This will print to original print stream;
        original.println("Done!");  // output to stdout

    }

    public static void StatCmp() throws IOException {
        String[] algorithms = {"NSGAII", "NSGAIII", "eMOEA"};

        //setup the experiment
        Executor executor = new Executor()
                .withProblemClass(MOEoC.class, clsfNames.length, kFolds, cvModelPath, trnCvFold, tstCvFold, clsfNames, trnFullData)
                .withMaxEvaluations(10000);

        Analyzer analyzer = new Analyzer()
                .withProblemClass(MOEoC.class, clsfNames.length, kFolds, cvModelPath, trnCvFold, tstCvFold, clsfNames, trnFullData)
                .includeHypervolume()
                .showStatisticalSignificance();

        //run each algorithm for 30/nRun seeds
        for (String algorithm : algorithms) {
            analyzer.addAll(algorithm,
                    executor.withAlgorithm(algorithm).runSeeds(nRun));
        }

//print the results
        analyzer.printAnalysis();

        File f = new File(resPath + expName + "StatAnalysis.txt");
        analyzer.saveAnalysis(f);

    }

    public static void NSGAIIexecute(String resFile) throws Exception {
        System.setOut(new PrintStream(new FileOutputStream(resFile)));
        System.out.println(headerTxt + "\n================================================================\n");

        // Step 4: Solve the MOEoC problem with 20 base classifiers: NSGAII
        //	public MOEoC(int numberOfBits, String cvModelPath, Instances[] trn,
        //	Instances[] tst, String[] clsf, Instances dataTrn)
        NondominatedPopulation resultNSGA = new Executor()
                .withProblemClass(MOEoC.class, clsfNames.length, kFolds, cvModelPath, trnCvFold, tstCvFold, clsfNames, trnFullData)
                .withAlgorithm("NSGAII")
                .withMaxEvaluations(10000)
                .distributeOnAllCores()
                .checkpointEveryIteration()
                .withCheckpointFile(new File(expName + "-NSGAII.chkpt"))
                .run();

        //  This will print to original print stream;
        original.println("NSGAII Execution Completed...");  // output to stdout
        System.out.println("\nNSGAII Solutions:");
        List<String> nonDominatedEnsembles = new ArrayList<String>();
        List<Integer> solnLen = new ArrayList<Integer>();

        for (Solution solution : resultNSGA) {
            nonDominatedEnsembles.add(solution.getVariable(0).toString());
            solnLen.add((int) (solution.getObjective(1)));
            System.out.println(solution.getVariable(0) + " "
                    + -(solution.getObjective(0)) + " "
                    + (int) solution.getObjective(1));
        }
        evaluateSolutions(nonDominatedEnsembles, solnLen);
    }

    public static void NSGAIIIexecute(String resFile) throws Exception {
        System.setOut(new PrintStream(new FileOutputStream(resFile)));
        System.out.println(headerTxt + "\n================================================================\n");

        // Step 4: Solve the MOEoC problem with 20 base classifiers: NSGAII
        //	public MOEoC(int numberOfBits, String cvModelPath, Instances[] trn,
        //	Instances[] tst, String[] clsf, Instances dataTrn)
        NondominatedPopulation resultNSGA = new Executor()
                .withProblemClass(MOEoC.class, clsfNames.length, kFolds, cvModelPath, trnCvFold, tstCvFold, clsfNames, trnFullData)
                .withAlgorithm("NSGAIII")
                .withMaxEvaluations(10000)
                .distributeOnAllCores()
                .checkpointEveryIteration()
                .withCheckpointFile(new File(expName + "-NSGAIII.chkpt"))
                .run();

        //  This will print to original print stream;
        original.println("NSGAIII Execution Completed...");  // output to stdout
        System.out.println("\nNSGAIII Solutions:");
        List<String> nonDominatedEnsembles = new ArrayList<String>();
        List<Integer> solnLen = new ArrayList<Integer>();

        for (Solution solution : resultNSGA) {
            nonDominatedEnsembles.add(solution.getVariable(0).toString());
            solnLen.add((int) (solution.getObjective(1)));
            System.out.println(solution.getVariable(0) + " "
                    + -(solution.getObjective(0)) + " "
                    + (int) solution.getObjective(1));
        }
        evaluateSolutions(nonDominatedEnsembles, solnLen);
    }

    public static void eMOEAexecute(String resFile) throws Exception {

        System.setOut(new PrintStream(new FileOutputStream(resFile)));
        System.out.println(headerTxt + "\n================================================================\n");

        List<String> nonDominatedEnsembles = new ArrayList<String>();
        List<Integer> solnLen = new ArrayList<Integer>();

        // solve the MOEoC problem with 20 base classifiers: eMOEA
        NondominatedPopulation resulteMOEA = new Executor()
                .withProblemClass(MOEoC.class, clsfNames.length, kFolds, cvModelPath, trnCvFold, tstCvFold, clsfNames, trnFullData)
                .withAlgorithm("eMOEA")
                .withMaxEvaluations(10000)
                .distributeOnAllCores()
                .checkpointEveryIteration()
                .withCheckpointFile(new File(expName + "-eMOEA.chkpt"))
                .run();

        // sort the results so the solutions appear in order
        // result.sort(new LexicographicalComparator());
        System.out.println("\neMOEA Solutions:");
        for (Solution solution : resulteMOEA) {
            nonDominatedEnsembles.add(solution.getVariable(0).toString());
            solnLen.add((int) (solution.getObjective(1)));

            System.out.println(solution.getVariable(0) + " "
                    + -(solution.getObjective(0)) + " "
                    + (int) solution.getObjective(1));
        }
        evaluateSolutions(nonDominatedEnsembles, solnLen);
    }

    public static void evaluateSolutions(List<String> solnCmb, List<Integer> solnLen) throws Exception {
        if (isEval == true) {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(strTstFile);
            tstFullData = source.getDataSet();
            tstFullData.setClassIndex(tstFullData.numAttributes() - 1);

            original.println("Evaluating Solutions...");  // output to stdout
            System.out.println("\nEvaluating Solutions...");
            System.out.println("Ensemble\tNo.of.Clsf\tPrecision\tAccuracy\tF-Measure\tMCC\tTP\tTN\tFP\tFN");
            int numSoln = solnCmb.size();
            for (int i = 0; i < numSoln; i++) {
                // EvaluateEnsembleOfClassifier(boolean dbg, String modelPath,
                // Instances trainData, Instances testData, String [] classifierNames)
                String theCmb = solnCmb.get(i);
                original.println("Evaluating: " + theCmb);  // output to stdout

                EvaluateEnsembleOfClassifier eoc = new EvaluateEnsembleOfClassifier(false, modelPath, trnFullData, tstFullData, clsfNames);
                eoc.testModel(theCmb);
                Evaluation eval = eoc.getTestEvaluation();
                int tp = (int) eval.numTruePositives(0);
                int tn = (int) eval.numTrueNegatives(0);
                int fp = (int) eval.numFalsePositives(0);
                int fn = (int) eval.numFalseNegatives(0);

                System.out.println(theCmb + "\t" + solnLen.get(i) + "\t" + eval.precision(0) + "\t" + eval.pctCorrect() + "\t" + eval.fMeasure(0) + "\t" + eval.matthewsCorrelationCoefficient(0) + "\t" + tp + "\t" + tn + "\t" + fp + "\t" + fn);
            }
        } else //10-foldCV Evaluation Results
        {
            original.println("Evaluating Solutions...");  // output to stdout
            System.out.println("\nEvaluating Solutions...");
            System.out.println("Ensemble\tNo.of.Clsf\tPrecision\tAccuracy\tF-Measure\tMCC\tTP\tTN\tFP\tFN");
            int numSoln = solnCmb.size();
            for (int i = 0; i < numSoln; i++) {
                String theCmb = solnCmb.get(i);
                original.println("Evaluating: " + theCmb);  // output to stdout
                //  public EvaluateEnsembleOfClassifier(boolean dbg, String cvModelPath,
                //  Instances[] cvTrnData, Instances[] cvTstData, String[] classifierNames, int fold)

                EvaluateEnsembleOfClassifier eoc = new EvaluateEnsembleOfClassifier(false, cvModelPath, trnCvFold, tstCvFold, clsfNames, kFolds);
                //String cmb, int kFold, Instances trnData
                eoc.crossValidate(theCmb, kFolds, trnFullData);
                Evaluation eval = eoc.getCVEvaluation();
                int tp = (int) eval.numTruePositives(0);
                int tn = (int) eval.numTrueNegatives(0);
                int fp = (int) eval.numFalsePositives(0);
                int fn = (int) eval.numFalseNegatives(0);

                System.out.println(theCmb + "\t" + solnLen.get(i) + "\t" + eval.precision(0) + "\t" + eval.pctCorrect() + "\t" + eval.fMeasure(0) + "\t" + eval.matthewsCorrelationCoefficient(0) + "\t" + tp + "\t" + tn + "\t" + fp + "\t" + fn);
            }
        }
        System.out.print("\n");
        System.setOut(new PrintStream(System.out));
    }

}
