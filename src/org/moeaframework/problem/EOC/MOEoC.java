/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moeaframework.problem.EOC;

import java.util.Random;

import org.gaeocframework.ensemble.EvaluateEnsembleOfClassifier;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.problem.AbstractProblem;

import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class MOEoC extends AbstractProblem {

    /**
     * The length of the bit string.
     */
    protected final int numberOfBits;
    protected final int kFolds;
    protected final String cvModelPath;
    protected final Instances[] trnCvFold;
    protected final Instances[] tstCvFold;
    protected final Instances trnData;
    protected Evaluation eval;
    protected final String[] clsfNames;
    protected boolean isDebug=false;
    
    /**
     * Constructs an instance of the LOTZ problem with the specified number of
     * bits.
     *
     * @param numberOfBits the number of bits
     */
    public void enableDebug()
    {
        isDebug=true;
    }
    
    public MOEoC() {
        this(20);
    }

    public MOEoC(int numberOfBits) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = "";
        this.trnCvFold = null;
        this.tstCvFold = null;
        this.clsfNames = null;
        this.trnData = null;
        this.kFolds = 10;
    }

    public MOEoC(int numberOfBits, String path) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = path;
        this.trnCvFold = null;
        this.tstCvFold = null;
        this.clsfNames = null;
        this.trnData = null;
        this.kFolds = 10;
    }

    public MOEoC(int numberOfBits, String path, Instances[] trn) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = path;
        this.trnCvFold = trn;
        this.tstCvFold = null;
        this.clsfNames = null;
        this.trnData = null;
        this.kFolds = 10;
    }

    public MOEoC(int numberOfBits, String path, Instances[] trn, Instances[] tst) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = path;
        this.trnCvFold = trn;
        this.tstCvFold = tst;
        this.clsfNames = null;
        this.trnData = null;
        this.kFolds = 10;
    }

    public MOEoC(int numberOfBits, String path, Instances[] trn,
            Instances[] tst, String[] clsf) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = path;
        this.trnCvFold = trn;
        this.tstCvFold = tst;
        this.clsfNames = clsf;
        this.trnData = null;
        this.kFolds = 10;
    }

    public MOEoC(int numberOfBits, String path, Instances[] trn,
            Instances[] tst, String[] clsf, Instances dataTrn) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = path;
        this.trnCvFold = trn;
        this.tstCvFold = tst;
        this.clsfNames = clsf;
        this.trnData = dataTrn;
        this.kFolds = 10;
    }

    public MOEoC(int numberOfBits, int fold, String path, Instances[] trn,
            Instances[] tst, String[] clsf, Instances dataTrn) {
        super(1, 2, 2);
        this.numberOfBits = numberOfBits;
        this.cvModelPath = path;
        this.trnCvFold = trn;
        this.tstCvFold = tst;
        this.clsfNames = clsf;
        this.trnData = dataTrn;
        this.kFolds = fold;
    }

    @Override
    public String getName() {
        return "Multi Objective Ensemble of Classifier";
    }

    @Override
    public int getNumberOfVariables() {
        return 1;
    }

    @Override
    public int getNumberOfObjectives() {
        return 2;
    }

    @Override
    public int getNumberOfConstraints() {
        return 2;
    }

    @Override
    public Solution newSolution() {
        Solution solution = new Solution(numberOfVariables, numberOfObjectives,
                numberOfConstraints);
        solution.setVariable(0, EncodingUtils.newBinary(numberOfBits));	// ensemble combination
        
        /*solution.setVariable(1, EncodingUtils.newReal(0.0, 1.0));	//  precision
         solution.setVariable(2, EncodingUtils.newReal(0.0, 100.0));	//  accuracy
         solution.setVariable(3, EncodingUtils.newReal(0.0, 1.0));	//  f-measure
         */

        return solution;
    }

    public Evaluation getEvaluation() {
        return this.eval;
    }

    @Override
    public void evaluate(Solution solution) {
        // Get the Ensemble Combination
        String ensembleCmb = solution.getVariable(0).toString();

        // Objective value calculation for Objective Function 1: MCC
        double mcc = -2.0;
        EvaluateEnsembleOfClassifier myEoC = new EvaluateEnsembleOfClassifier(
                false, cvModelPath, trnCvFold, tstCvFold, clsfNames, kFolds);
        try {
            mcc = myEoC.crossValidate(ensembleCmb, kFolds, trnData);
            eval = myEoC.getCVEvaluation();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Objective value calculation for Objective Function 2: #base classifiers in the Ensemble Combination
        int size = 0;
        for (int i = 0; i < ensembleCmb.length(); i++) {
            if (ensembleCmb.charAt(i) == '1') {
                size++;
            }
        }

		// we want to constrain all objectives to be above
        // to a given level. Pull out the values of the objectives:
        double objA = mcc;
        int objB = size;

/*
        System.out.println("Eval: " + ensembleCmb + "\tMCC: " + objA + "\t#Clsf: "
                + objB);
*/
        // Set the desired level of the objectives:
        double levelA = 0.0; // greater than 0.0 [random classifier]
        int levelB = 2; // greater than 1 [at least two classifier in the
        // combination]

        // Now calculate the constraint violations.
        double[] constrs = new double[numberOfConstraints];

        // The 1st constraint is a greater than or equal to constraint on objA
        if (objA > levelA) {
            solution.setConstraint(0, 0.0);
        } else {
            solution.setConstraint(0, objA);
        }

        // The 2nd constraint is a greater than or equal to constraint on objB
        if (objB >= levelB) {
            solution.setConstraint(1, 0.0);
        } else {
            solution.setConstraint(1, numberOfBits - objB);
        }

        solution.setObjective(0, -mcc); // negate the fitness value (mcc) to
        // maximize it
        solution.setObjective(1, size);// objective is to minimize size	
    }
}
