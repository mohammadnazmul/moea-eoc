package org.gaeocframework.ensemble;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.misc.InputMappedClassifier;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class EvaluateEnsembleOfClassifier {

    EnsembleOfClassifier myEOC;
    boolean isDebug = false;
    String strCmb;
    boolean loadFromPath = false;

    // Test EoC
    String pathFullModels;
    Evaluation testEval;
    List<Classifier> preBuiltClsfModels = new ArrayList<Classifier>();
    Instances trainFullData;
    Instances testFullData;

    // Cross Validate EoC
    String pathCVModels;
    Evaluation trnCVEval;
    Instances[] tstCvFold;
    Instances[] trnCvFold;
    Classifier[][] preBuiltCVModels;
    int kFolds;

    String[] clsfNames;

    /*public static String[] clsfNames = new String[] { "BayesNet",
     "DecisionStump", "DecisionTable", "IBk", "J48", "JRip", "LibSVM",
     "LMT", "Logistic", "NaiveBayes", "NaiveBayesUpdateable", "OneR",
     "PART", "RandomForest", "RandomTree", "REPTree", "SGD",
     "SimpleLogistic", "VotedPerceptron", "ZeroR" };
     */
    /*
     * Cross Validate an EoC Combination
     * 
     * @param for Instantiate: debug, loaded serialized cv models/path to load
     * cv models, cv training data folds, cv testing data folds, classifierNames
     * 
     * @param for Execute: ensemble combination string, kFold, training dataset
     */
    // 1. loading cv models from path
    public EvaluateEnsembleOfClassifier(boolean dbg, String cvModelPath,
            Instances[] cvTrnData, Instances[] cvTstData, String[] classifierNames, int fold) {

        isDebug = dbg;
        tstCvFold = cvTstData;
        trnCvFold = cvTrnData;
        clsfNames = classifierNames;

        pathCVModels = cvModelPath;
        loadFromPath = true;
        kFolds = fold;

    }

    // 2. using pre-loaded cv models - pass them as parameter
    public EvaluateEnsembleOfClassifier(boolean dbg,
            Classifier[][] cvClsfModels, Instances[] cvTrnData,
            Instances[] cvTstData, int fold) {
        isDebug = dbg;
        preBuiltCVModels = cvClsfModels;
        tstCvFold = cvTstData;
        trnCvFold = cvTrnData;
        loadFromPath = false;
        kFolds = fold;

    }

    public double crossValidate(String cmb, int kFold, Instances trnData)
            throws Exception {
        trainFullData = trnData;
        kFolds = kFold;

        double fit = getEnsembleFitness(cmb);
        if (isDebug) {
            System.out.println("Fitness of the Ensemble Combination: " + cmb
                    + " = " + fit);
        }
        return fit;
    }

    private double getEnsembleFitness(String ensmCmb) throws Exception {
        double fit = -2.0;

        if (isDebug) {
            System.out.println("Start:" + kFolds
                    + "-fold Cross Validation on Training Set\n");
        }

        /*
         // randomize trainFullData
         Random rand = new Random(1);
         Instances randData = new Instances(trainFullData);
         randData.randomize(rand);
         if (randData.classAttribute().isNominal()) {
         randData.stratify(kFolds);
         }
         */
        trnCVEval = new Evaluation(trainFullData);
        for (int n = 0; n < kFolds; n++) {
            Evaluation eval = new Evaluation(trainFullData);
            Instances test = tstCvFold[n];

            // build and evaluate classifier
            preBuiltClsfModels = new ArrayList<Classifier>();
            if (loadFromPath == true) {
                loadCVCombination(ensmCmb, n);
            } else {
                setCVCombination(ensmCmb, n);
            }
            eval.evaluateModel(myEOC, test);
            trnCVEval.evaluateModel(myEOC, test);

            if (isDebug) {// output evaluation
                System.out.println();
                System.out.println(eval
                        .toMatrixString("=== Confusion matrix for fold "
                                + (n + 1) + "/" + kFolds + " ==="));
                System.out.println("MCC:" + eval.weightedMatthewsCorrelation());
            }
        }

        // output evaluation
        if (isDebug) {
            System.out.println();
            System.out.println(trnCVEval.toSummaryString("=== " + kFolds
                    + "-fold Cross-validation ===", false));
            System.out
                    .println("MCC:" + trnCVEval.weightedMatthewsCorrelation());
            System.out.println(trnCVEval.toMatrixString());
        }
        fit = trnCVEval.weightedMatthewsCorrelation();
        return fit;
    }

    // Now loading Prebuilt CV Models from path
    void loadCVCombination(String cmb, int fold) throws Exception {
        strCmb = cmb;
        myEOC = new EnsembleOfClassifier();
        for (int i = 0; i < cmb.length(); i++) {
            if (cmb.charAt(i) == '1') {
                String clsfPath = pathCVModels + clsfNames[i] + "-" + fold
                        + ".model";
                if (isDebug) {
                    System.out.println("Loading: " + clsfPath);
                }
                Classifier cls = (Classifier) weka.core.SerializationHelper
                        .read(clsfPath);
                preBuiltClsfModels.add(cls);
                if (isDebug) {
                    System.out.println("Loaded: "
                            + cls.getClass().getSimpleName());
                }
            } // END:if charAt
        } // END:for cmb.length
        myEOC.setPreBuiltClassifiers(preBuiltClsfModels);
    }

    // Now using Prebuilt CV Models provided as parameter
    private void setCVCombination(String cmb, int fold) throws Exception {
        strCmb = cmb;
        myEOC = new EnsembleOfClassifier();

        for (int i = 0; i < cmb.length(); i++) {
            if (cmb.charAt(i) == '1') {
                Classifier cls = preBuiltCVModels[fold][i];
                preBuiltClsfModels.add(cls);
            } // END:if charAt
        } // END:for cmb.length
        myEOC.setPreBuiltClassifiers(preBuiltClsfModels);
    }

    /*
     * Evaluate an EoC Combination
     * 
     * @param for Instantiate: debug, loaded serialized Models/path to load
     * Models, training dataset, testing dataset
     * 
     * @param for Execute: ensemble combination string, training dataset,
     * testing dataset
     */
    public EvaluateEnsembleOfClassifier(boolean dbg,
            List<Classifier> clsfModels, Instances trainData, Instances testData) {
        testFullData = testData;
        trainFullData = trainData;
        preBuiltClsfModels = clsfModels;
        loadFromPath = false;
    }

    public EvaluateEnsembleOfClassifier(boolean dbg, String modelPath,
            Instances trainData, Instances testData, String[] classifierNames) {
        testFullData = testData;
        trainFullData = trainData;
        pathFullModels = modelPath;
        clsfNames = classifierNames;

        loadFromPath = true;
    }

    public double testModel(String cmb) //, Instances trnData, Instances tstData)
            throws Exception {
        double fit;
 // Set Classifier Input Mapper
        InputMappedClassifier    inpMapper;
        inpMapper               = new InputMappedClassifier();
        String mapperArgs[]     = {"-I", "-trim", "-M"};
        
        // Set Ensemble Combination
        if (loadFromPath == true) {
            loadFullEnsemble(cmb);
        } else {
            setFullEnsemble(cmb);
        }
        
        inpMapper.setOptions(mapperArgs);
        inpMapper.setClassifier(myEOC);
        inpMapper.buildClassifier(trainFullData);
        
        testEval = new Evaluation(trainFullData);
        testEval.evaluateModel(inpMapper, testFullData);

        // output evaluation
        if (isDebug) {
            System.out.println();
            System.out.println(testEval
                    .toMatrixString("=== Confusion matrix for Ensemble of Classifier on Testing Dataset ==="));
            System.out.println(testEval.toSummaryString());
            System.out.println("MCC:" + testEval.weightedMatthewsCorrelation());
        }
        fit = testEval.weightedMatthewsCorrelation();

        return fit;
    }

    public Evaluation getTestEvaluation() {
        return testEval;
    }

    public Evaluation getCVEvaluation() {
        return trnCVEval;
    }

    private void setFullEnsemble(String cmb) throws Exception {
        strCmb = cmb;
        myEOC = new EnsembleOfClassifier();
        myEOC.setPreBuiltClassifiers(preBuiltClsfModels);
    }

    void loadFullEnsemble(String cmb) throws Exception {
        strCmb = cmb;
        myEOC = new EnsembleOfClassifier();
        preBuiltClsfModels = new ArrayList<Classifier>();

        for (int i = 0; i < cmb.length(); i++) {
            if (cmb.charAt(i) == '1') {
                String clsfPath = pathFullModels + clsfNames[i] + ".model";
                if (isDebug) {
                    System.out.println("Loading: " + clsfPath);
                }
                Classifier cls = (Classifier) weka.core.SerializationHelper
                        .read(clsfPath);
                preBuiltClsfModels.add(cls);
                if (isDebug) {
                    System.out.println("Loaded: "
                            + cls.getClass().getSimpleName());
                }
            } // END:if charAt
        } // END:for cmb.length
        myEOC.setPreBuiltClassifiers(preBuiltClsfModels);
    }
}
